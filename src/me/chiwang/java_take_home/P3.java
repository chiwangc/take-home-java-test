package me.chiwang.java_take_home;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class P3 extends JFrame {
	
	public P3() {
		initUI();
	}
	
	private void initUI() {
		
		setTitle("Rejected Orders");
		setSize(600, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		P2.main(null);
		
		JPanel content = new JPanel();
        content.setLayout(new BorderLayout());        
				
		JTextArea textArea = new JTextArea(P2.getStringForPrinting());
			textArea.setFont(new Font("Serif", Font.PLAIN, 16));
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);

		textArea.setCaretPosition(textArea.getDocument().getLength());
			
		JScrollPane areaScrollPane = new JScrollPane(textArea);
		areaScrollPane.setVerticalScrollBarPolicy(
		                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		content.add(areaScrollPane, BorderLayout.CENTER);


        this.add(content);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				P3 ui = new P3();
				ui.setVisible(true);
			}
		});
	}
}

