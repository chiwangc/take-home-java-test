package me.chiwang.java_take_home;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

enum SequenceState {
	ASCENDING, DESCENDING, UNCHANGE
}

public class P1 {
	
	public static void main(String[] args) {
		Path currentRelativePath = Paths.get("data/Data1.txt");
		final String inputFilePath = currentRelativePath.toAbsolutePath().toString();
		
		try (BufferedReader br = new BufferedReader(new FileReader(inputFilePath))) {
			String line;
			while ((line = br.readLine()) != null) {
				findSubSequence(line);
			}			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}		
	}
	
	private static void findSubSequence(String sequenceOfNums) {
		
		Scanner scanner = new Scanner(sequenceOfNums);
		scanner.useDelimiter(", ");
		
		// Result
		int lenghtOfBestSubSeq = 1;
		int subSeqStartIndex = 0;
		
		// Helper variables
		SequenceState seqState = SequenceState.UNCHANGE;
		int previousValue = scanner.nextInt();
		int currentIndex = 0;
		int currentValue = 0;
		int currentSeqStartIndex = 0;
		int currentSeqLength = 1;		
		
		while (scanner.hasNext()) {
			currentIndex++;
			currentValue = scanner.nextInt();
			
			switch (seqState) {
			case ASCENDING:
				if (currentValue > previousValue) {
					// Still continuing the trend
					currentSeqLength++;
				} else {	// Change of monotonicity
					// Update current best
					if (currentSeqLength > lenghtOfBestSubSeq) {
						lenghtOfBestSubSeq = currentSeqLength;
						subSeqStartIndex = currentSeqStartIndex;
					}
					
					// Reset					
					if (currentValue == previousValue) {
						seqState = SequenceState.UNCHANGE;
						currentSeqLength = 1;
						currentSeqStartIndex = currentIndex;
					} else {
						// In this case we have currentValue < previousValue
						seqState = SequenceState.DESCENDING;
						currentSeqLength = 2;
						currentSeqStartIndex = currentIndex - 1;
					}
				}
				break;
			case DESCENDING:
				if (currentValue < previousValue) {
					// Still continuing the trend
					currentSeqLength++;
				} else {	// Change of monotonicity
					// Update current best
					if (currentSeqLength > lenghtOfBestSubSeq) {
						lenghtOfBestSubSeq = currentSeqLength;
						subSeqStartIndex = currentSeqStartIndex;
					}
					
					// Reset					
					if (currentValue == previousValue) {
						seqState = SequenceState.UNCHANGE;
						currentSeqLength = 1;
						currentSeqStartIndex = currentIndex;
					} else {
						// In this case we have currentValue > previousValue
						seqState = SequenceState.ASCENDING;
						currentSeqLength = 2;
						currentSeqStartIndex = currentIndex - 1;
					}
				}
				break;
			case UNCHANGE:
				if (currentValue == previousValue) {
					// Remain unchange
					currentSeqStartIndex = currentIndex;
				} else {
					currentSeqLength++;
					if (currentValue < previousValue) {
						seqState = SequenceState.DESCENDING;
					} else {
						seqState = SequenceState.ASCENDING;
					}
				}
				break;
			default:
				// This is impossible
				break;
			}
			
			previousValue = currentValue;
		}
		
		// Update current best for the very last element
		if (currentSeqLength > lenghtOfBestSubSeq) {
			lenghtOfBestSubSeq = currentSeqLength;
			subSeqStartIndex = currentSeqStartIndex;
		}
		
		scanner.close();
		
		
		System.out.printf("The input sequence is: %s%n", sequenceOfNums);
		System.out.printf("The start index of the longest ascending or descending contiguous subsequence is %d%n", subSeqStartIndex);
		String[] parts = sequenceOfNums.split(", ");
		System.out.printf("The subsequence is:%n");
		for (int i = 0; i < lenghtOfBestSubSeq; i++) {
			if (i != lenghtOfBestSubSeq - 1) {
				System.out.print(parts[subSeqStartIndex + i] + ", ");
			} else {
				System.out.println(parts[subSeqStartIndex + i]);
			}
			
		}
		System.out.println("==========================================");
	}

}
