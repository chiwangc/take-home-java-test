package me.chiwang.java_take_home;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class P2 {
	
	private static final Pattern pattern1 = Pattern.compile("(\\d+) (\\w+):REJ:(\\w+),(\\w+),(\\w+)");
	private static final Pattern pattern2 = Pattern.compile("(\\d+) (\\w+): error : Rejected: (\\w+) (\\w+) (\\w+)");
	
	// Results
	private static Map<String, ExchangeRecord> exchangeMap;
	private static Map<String, Map<String, Integer>> contractMap;
		
	public static void main(String[] args) {
		// Initialization
		exchangeMap = new HashMap<>();
		contractMap = new HashMap<>();
		
		Path currentRelativePath = Paths.get("data/rejectedorders.log");
		final String inputFilePath = currentRelativePath.toAbsolutePath().toString();
		
		try (BufferedReader br = new BufferedReader(new FileReader(inputFilePath))) {
			String line;
			while ((line = br.readLine()) != null) {
				updateRecord(line);
			}			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		
		// Print the results
		System.out.println(getStringForPrinting());
		
	}
	
	public static String getStringForPrinting() {
		StringBuilder sb = new StringBuilder();
		
		Iterator<Entry<String, ExchangeRecord>> it1 = exchangeMap.entrySet().iterator();
		while (it1.hasNext()) {
			Map.Entry<String, ExchangeRecord> element = it1.next();
			String exchange = element.getKey();
			ExchangeRecord record = element.getValue();
			
			sb.append("Exchange Name: " + exchange + "\n" + record);
			sb.append("===========================\n");
		}
		
		Iterator<Entry<String, Map<String, Integer>>> it2 = contractMap.entrySet().iterator();
		while (it2.hasNext()) {
			Map.Entry<String, Map<String, Integer>> element = it2.next();
			sb.append("ContractID: " + element.getKey() + "\n");
			
			Iterator<Entry<String, Integer>> it3 = element.getValue().entrySet().iterator();
			while (it3.hasNext()) {
				Entry<String, Integer> entry = it3.next();
				sb.append("\t Reject Type: " + entry.getKey() + ", count = " + entry.getValue() + "\n");
			}
			sb.append("---------------------------\n");
		}
		return sb.toString();
	}
	
	private static void updateRecord(String lineOfRecord) {
		
		Matcher matcher1 = pattern1.matcher(lineOfRecord);
		Matcher matcher2 = pattern2.matcher(lineOfRecord);
		
		int timestamp = 0;
		String exchange = null;
		String type = null;
		String contractID = null;
		String orderID = null;
		
		if (matcher1.matches()) {
			// Case 1
			timestamp = Integer.parseInt(matcher1.group(1));
			exchange = matcher1.group(2);
			type = matcher1.group(3);
			contractID = matcher1.group(4);
			orderID = matcher1.group(5);			
		} else if (matcher2.matches()) {
			// Case 2
			timestamp = Integer.parseInt(matcher2.group(1));
			exchange = matcher2.group(2);
			type = matcher2.group(5);
			contractID = matcher2.group(3);
			orderID = matcher2.group(4);
		} else {
			throw new InputMismatchException("This is an illegal format for a record");
		}
				
		// Update exchangeMap
		ExchangeRecord er;
		
		if (exchangeMap.containsKey(exchange)) {
			er = exchangeMap.get(exchange);
		} else {
			er = new ExchangeRecord();
			exchangeMap.put(exchange, er);
		}
		er.rejectionReceived(timestamp);
		
		// Update contractMap
		Map<String, Integer> typeMap;
		
		if (contractMap.containsKey(contractID)) {
			typeMap = contractMap.get(contractID);
		} else {
			typeMap = new HashMap<String, Integer>();
			contractMap.put(contractID, typeMap);
		}
		
		if (typeMap.containsKey(type)) {
			typeMap.put(type, typeMap.get(type) + 1);
		} else {
			typeMap.put(type, 1);
		}

	}
	
	private static class ExchangeRecord {
		private int rejectionCount = 0;
		private int lastRejectionTime = 0;
		
		public void rejectionReceived(int time) {
			rejectionCount++;
			lastRejectionTime = time;
		}
		
		@Override
		public String toString() {
			String s1 = String.format("Total number of rejection: %d%n", rejectionCount);
			String s2 = String.format("Last rejection time: %d%n", lastRejectionTime);
			return s1 + s2;
		}
	}

}
